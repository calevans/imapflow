<?php

declare(strict_types=1);

use Pimple\Container;
use GuzzleHttp\Client;

$container = new Container();
$configFunc = require_once $appPath . "config/config.php";
$config = $configFunc($appPath);

$container['guzzle'] = new Client();
$container['config'] = $config;

$container['accounts'] = function ($c) {
    return include $c['config']['appPath'] . "config/accounts.php";
};

return $container;
