<?php
declare(strict_types=1);

return function ($appPath) {
  $appPath = trim($appPath);
  $appPath = substr($appPath, 0, -1) === '/' ? $appPath : $appPath . '/';
  return [
    'appPath' => $appPath,
    'configPath' => $appPath . 'config',
  ];
};