# IMAPFlow

Poll an IMAP email account, fetch new emails not yet processed, and pipe them to a script.

# License
MIT

# Before you begin

- If you do not know what IMAP is, this is not the project for you.
- If you do not know what REST is, this is not the project for you.
- If you do not know PHP, this is not the project for you.

I do not reply to issues. If you have an issue, fork it, fix it and feel free to PR it back to me.

I do look at all PRs, but decide on a case by case basis wether I include them in the main codebase. Basically, this is a "It works for me" project. You are welcome to use it but you are 100% on your own. By using this project, you agree to this simple rule.

# Install
- `git clone` the project on to the server you want to run it on
- `composer install` to get all the required libraries.
- Create your `config/accounts.php`. Just rename `config/accounts.php.sample` and then change the values. A note on this. Each account represents an email account your want to log into and process email. This program WILL delete any emails that are successfully processed. If you want to filter the emails by who sent them, put the emails in the `validSenders` array for the account. If the `validSenders` array is empty, it will process **ALL** emails in the mailbox.

# Running
``