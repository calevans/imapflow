#!/usr/bin/env php
<?php
declare(strict_types=1);

use Symfony\Component\Console\Application;

$appPath = realpath(dirname(__FILE__) ) . '/';

require_once $appPath . 'vendor/autoload.php';

use IMAPFlow\Commands\IMAPFlowCommand;

$container = require_once $appPath . 'bootstrap.php';
$app = new Application('IMAPFlow', '1.0.0');
$app->container = $container;

$flow = new IMAPFLowCommand();

$app->addCommands([$flow]);

$app->setDefaultCommand( $flow->getName() );

try {
  $app->run();
} catch (\Throwable $e) {
  error_log($e->getMessage()??'NO MESSAGE');
}

