<?php

declare(strict_types=1);

namespace IMAPFLow\Models;

use ZBateson\MailMimeParser\Message;

class Pipe
{
  public function __construct(protected Message $email, protected string $pipe)
  {
  }

  public function flow(Email $email): void
  {
  }
}
