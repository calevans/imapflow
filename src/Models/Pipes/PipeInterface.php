<?php

declare(strict_types=1);

namespace IMAPFlow\Models\Pipes;

use ZBateson\MailMimeParser\Message;
use GuzzleHttp\Client;

interface PipeInterface
{
  public function __construct(Client $client = null);

  public function __invoke(
      Message $message,
      array $config,
      bool $debug = false
  ): bool;
}
