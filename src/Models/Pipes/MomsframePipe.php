<?php

declare(strict_types=1);

namespace IMAPFlow\Models\Pipes;

use ZBateson\MailMimeParser\Message\MimePart;
use IMAPFlow\Models\Pipes\WpmediaPipe;

/**
 * Represents a post to a WP Media Rest Endpoint specifically for my MomsFrame
 * project. We have to pull out some extra data for the meta data to send in.
 * Also, we do validation here to make sure that the sender has permission
 * to send to the device and that the receiver is actually a device.
 */
class MomsframePipe extends WpmediaPipe
{
  public const DEVICE_CATEGORY = 6;
  public const HOLIDAY_CATEGORY = 7;

  protected function sendMediaToWordPress(MimePart $attachment): object
  {
      $deviceId = $this->validateDevice($this->message->getHeader('To')->getEmail());

    if ($deviceId === 0) {
        throw new \Exception('Invalid Device', -1);
    }

      $senderId = $this->validateSender(
          $this->message->getHeader('From')->getEmail(),
          $this->message->getHeader('To')->getEmail()
      );

    if ($senderId === 0) {
        throw new \Exception('Invalid Sender', -2);
    }

      $response = parent::sendMediaToWordPress($attachment);

      $hashtags = $this->parseHashTags($this->message->getTextContent());

      $mediaId = $response->id;

      $newPayload = [];
      $newPayload['headers']['Authorization'] = $this->config['auth']['value'];
      $newPayload['form_params']['author'] = $senderId;
      $newPayload['form_params']['categories'] = array_merge(
          [$deviceId],
          $hashtags['devices'],
          $hashtags['holidays']
      );

      $response = $this->client->put(
          $this->config['url'] . 'wp-json/wp/v2/media/' . $mediaId,
          $newPayload
      );

      return $response;
  }

  protected function parseHashTags(string $messageText): array
  {
      $returnValue = [
        'devices' => [],
        'holidays' => []
      ];

      $deviceIds = $this->fetchTaxonomyFamily(
          self::DEVICE_CATEGORY,
          'categories'
      );
      $holidayIds = $this->fetchTaxonomyFamily(
          self::HOLIDAY_CATEGORY,
          'categories'
      );


    foreach (explode(PHP_EOL, $messageText) as $value) {
        $value = trim($value);
      if (substr($value, 0, 1) !== '#') {
        continue;
      }
        $value = trim($value, '#');
        $thisTaxonimyId = $this->fetchTaxonomyId($value, 'categories');

      if ($thisTaxonimyId === 0) {
          continue;
      }
      if (in_array($thisTaxonimyId, $deviceIds)) {
          $returnValue['devices'][] = $thisTaxonimyId;
          continue;
      }
      if (in_array($thisTaxonimyId, $holidayIds)) {
          $returnValue['holidays'][] = $thisTaxonimyId;
          continue;
      }
        // not a holiday and not a device...don't care anymore.
    }
      return $returnValue;
  }
  protected function validateDevice(string $deviceName): int
  {
      $deviceIds = $this->fetchTaxonomyFamily(
          self::DEVICE_CATEGORY,
          'categories'
      );

    try {
      $deviceId = $this->fetchTaxonomyId($deviceName, 'categories');
    } catch (\Exception $e) {
        return 0;
    }
      return $deviceId;
  }

    /**
     * Validate that the sender has an account and that they have access to
     * this device.
     */
  protected function validateSender(
      string $emailAddress,
      string $deviceName
  ): int {

      $response = $this->client->request(
          'GET',
          $this->config['url'] .
          'wp-json/wp/v2/users?context=edit&search=' .
          $emailAddress,
          [
            'headers' => [
              'Accept'     => 'application/json',
              'Authorization' => $this->config['auth']['value']
            ],
            'debug' => $this->debug,
            'http_errors' => $this->debug
          ]
      );

      $decoded = json_decode($response->getBody()->getContents());

      /*
       * Do we have any matches?
       */
    if (count($decoded) < 1) {
        return 0;
    }

      $thisUser = $decoded[0];

      /*
     * Is the email address an EXACT MATCH?
     */
    if ($thisUser->email !== $emailAddress) {
        return 0;
    }

      /*
       * Does this user have access to this device?
       */
      $deviceName = strtolower($deviceName);
    if (! is_array($thisUser->acf->associated_device) || ! \in_array($deviceName, $thisUser->acf->associated_device)) {
        return 0;
    }

      /*
     * Does this user have the ability to upload files?
     */
    if (! isset($thisUser->capabilities->upload_files) || $thisUser->capabilities->upload_files === false) {
        return 0;
    }

      return $thisUser->id;
  }


  protected function validateCategories(array $categories): array
  {
      // return a modified array that is jsut the valid categories?
      // Should this be it's own class?
  }
}
