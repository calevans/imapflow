<?php

declare(strict_types=1);

namespace IMAPFLow\Models\Pipes;

use IMAPFlow\Models\Pipes\RestPipe;
use ZBateson\MailMimeParser\Message\MimePart;

/**
 * Pull the images in an email out and post them to a WordPress Media Rest
 * Endpoint
 *
 * This takes a message in the __invoke() and builds the payload and then
 * executes the POST. Then it executes a PATCH to send any extra data.
 *
 * This is ONLY for messages with media attachments. It should check the
 * attachment's Content-Type and throw an exception on any one that does not
 * match the whitelist.
 */
class WPMediaPipe extends RestPipe
{
  protected function process(): bool
  {

    for ($lcvA = 0; $lcvA < $this->message->getAttachmentCount(); $lcvA++) {
        $this->sendMediaToWordPress($this->message->getAttachmentPart($lcvA));
    }

      return true;
  }

  /*
   * Send a single media file to WordPress
   */
  protected function sendMediaToWordPress(MimePart $attachment): object
  {

      $this->initializePayload();
      $this->addPart(
          'file',
          $attachment->getContentStream(),
          [
            'filename' =>  $attachment->getFilename(),
            'headers'  => [
              'Content-Type' => $attachment->getContentType()
            ]
          ]
      );

      $response = $this->client->post(
          $this->config['url'] . 'wp-json/wp/v2/media',
          $this->payload
      );
      return json_decode($response->getBody()->getContents());
  }

  /**
   * Fetch the ids of
   * e.g given that holiday is category id 7, get a list of all categories
   * that are under holiday.
   */
  protected function fetchTaxonomyFamily(
      int $value,
      string $filter
  ): array {

    switch (trim($filter)) {
      case 'tags':
        $display = 'Tags';
        $endpoint = 'tags';
          break;
      case 'categories':
          $display = 'Categories';
          $endpoint = 'categories';
          break;
      default:
          throw new \Exception($filter . ' is not a valid taxonomy type.');
    }

    $response = $this->client->request(
        'GET',
        $this->config['url'] . 'wp-json/wp/v2/' . $endpoint . '?parent=' . $value,
        [
          'headers' => [
            'Accept'     => 'application/json',
            'Authorization' => $this->config['auth']['value']
          ],
          'debug' => $this->debug,
          'http_errors' => $this->debug
        ]
    );
      $decoded = json_decode($response->getBody()->getContents());

    if (count($decoded) < 1) {
        throw new \Exception($value . ' is not a valid ' . $display);
    }

      $returnValue = array_map(
          function ($value) {
              return $value->id;
          },
          $decoded
      );
      return $returnValue;
  }

  /**
   * Fetch the id of a specific taxonomy item
   */
  public function fetchTaxonomyId(
      string $value,
      string $filter
  ): int {

    switch (trim($filter)) {
      case 'tags':
        $display = 'Tags';
        $endpoint = 'tags';
          break;
      case 'categories':
          $display = 'Categories';
          $endpoint = 'categories';
          break;
      default:
          throw new \Exception($filter . ' is not a valid taxonomy type.');
    }

    $response = $this->client->request(
        'GET',
        $this->config['url'] . 'wp-json/wp/v2/' . $endpoint . '?search=' . $value,
        [
          'headers' => [
            'Accept'     => 'application/json',
            'Authorization' => $this->config['auth']['value']
          ],
          'debug' => $this->debug,
          'http_errors' => $this->debug,
        ]
    );

    $decoded = json_decode($response->getBody()->getContents());

    if (count($decoded) < 1) {
        return 0;
    }
      return $decoded[0]->id;
  }
}
