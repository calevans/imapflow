<?php

declare(strict_types=1);

namespace IMAPFlow\Models\Pipes;

use GuzzleHttp\Client;
use IMAPFlow\Models\Pipes\PipeInterface;

class PipeFactory
{
  public static function create(string $name, Client $client = null): PipeInterface
  {
      $client = $client ?? new Client();
      $class = 'IMAPFlow\\Models\\Pipes\\' . ucfirst($name) . 'Pipe';
      return new $class($client);
  }
}
