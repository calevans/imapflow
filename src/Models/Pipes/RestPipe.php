<?php

declare(strict_types=1);

namespace IMAPFlow\Models\Pipes;

use IMAPFlow\Models\Pipes\AbstractPipe;
use ZBateson\MailMimeParser\Message;

  /*
   * Represents a Generic REST POST
   *
   * This takes a message in the __invoke() and builds the payload and then
   * executes the POST.
   *
   * This should work for any proper REST endpoint. It is up to the Endpoint to
   * know how to deal with the attachments and their types.
   */
class RestPipe extends AbstractPipe
{
  protected function process(): bool
  {
      $fromEmail = $this->message->getHeader('From')->getEmail();
      $toEmail = $this->message->getHeader('To')->getEmail();

      $this->initializePayload($this->debug);

    foreach ($this->message->getAllHeaders() as $header) {
        $this->addPart($header->getName(), $header->getValue());
    }

      $this->addPart('BodyHtml', $this->message->getHtmlContent());
      $this->addPart('BodyText', $this->message->getTextContent());

      $this->addAttachments($this->message);

      $this->setAuth($this->config);

    try {
        $this->client->post($this->config['url'], $this->payload);
    } catch (\Throwable $e) {
        echo $e->getResponse()->getBody()->getContents();
        return false;
    }

    return true;
  }


  /*
  * Initialize the array
  */
  protected function initializePayload(): void
  {
    $this->payload =  [
      'debug' => $this->debug,
      'multipart' => [],
      'headers' => []
    ];
    $this->setAuth();
  }


  /**
   * REST SPECIFIC...move into RestPipe
   * Set the auth header if it exists.
   *
   * In most cases this is going to be a Basic Auth header. It could however
   * be a Bearer token or some other type of auth header. We'll just set it
   * and let the server decide if it's valid or not.
   */
  protected function setAuth(): void
  {
    if (isset($this->config['auth']['value']) && ! empty($this->config['auth']['value'])) {
      $this->payload['headers']['Authorization'] = $this->config['auth']['value'];
    }
  }

  /**
   * Adds all of the attachments to the payload
   */
  public function addAttachments(Email $message): void
  {
    for ($lcvA = 0; $lcvA < $message->getAttachmentCount(); $lcvA++) {
      $attachment = $message->getAttachmentPart($lcvA);
      $this->addPart(
          $attachment->getFilename(),
          $attachment->getContentStream(),
          [
            'filename' =>  $attachment->getFilename(),
            'headers'  => [
              'Content-Type' => $attachment->getContentType()
            ]
          ]
      );
    }
  }


  /*
   * Add a record to the payload multipart array.
   * If the name already exists, append it, otherwise, create a new entry.
   * New entries can also have an optional array of other parameters merged in.
   * These are headers, etc.
   */
  protected function addPart(string $key, $value, array $optional = []): void
  {
    foreach ($this->payload['multipart'] as $currentKey => $currentValue) {
      if ($currentValue['name'] === $key) {
        $this->payload['multipart'][$currentKey]['contents'] .=  "\n" . $value;
        return;
      }
    }

    $results = [
      'name' => $key, // This has to be 'file' for WordPress...INORITE?!?
      'contents' => $value
    ];

    $this->payload['multipart'][] = array_merge($results, $optional);
  }
}
