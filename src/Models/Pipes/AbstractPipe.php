<?php

namespace IMAPFlow\Models\Pipes;

use guzzlehttp\client;
use IMAPFlow\Models\Pipes\PipeInterface;
use ZBateson\MailMimeParser\Message;

abstract class AbstractPipe implements PipeInterface
{
  protected Message $message;
  protected array $config = [];
  protected bool $debug = false;

  public function __construct(protected ?Client $client = null)
  {
  }

  public function __invoke(
      Message $message,
      array $config,
      bool $debug = false
  ): bool {
      $this->message = $message;
      $this->config = $config;
      $this->debug = $debug;

      return $this->process();
  }

  abstract protected function process(): bool;
}
