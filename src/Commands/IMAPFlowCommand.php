<?php

/**
 * @todo implement a real logger instead of passing around the output.
 */

declare(strict_types=1);

namespace IMAPFlow\Commands;

use Ddeboer\Imap\Server;
use Ddeboer\Imap\Connection;
use Ddeboer\Imap\MailboxInterface;
use IMAPFlow\Models\Pipes\PipeFactory;
use ZBateson\MailMimeParser\Message;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

/**
 * IMAPFlowCommand
 *
 * This is the main entry point for the IMAPFlow system.
 *
 * Once run it will:
 * 1. Read the list of email accounts we monitor
 * 2. In a loop, open each email account
 * 3. In a loop, read each email in the account
 * 4. Pipe the email to the proper pipes
 * 5. Delete the email
 * 6. Repeat
 * 7. Shutdown
 *
 * I envision this being calls via a cron job every minute or so.
 */
class IMAPFlowCommand extends Command
{
  protected bool $debug = false;

  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
      $definition = [];

      $this->setName('flow')
         ->setDescription('Reads an email account and processes the emails found.')
         ->setDefinition($definition)
         ->setHelp('Reads the config fil and polls all applicable IMAP accounts.'
         . 'Reads in new emails, pipes them to the proper pipe, and then deletes them.');
      return;
  }

  /**
   * Main body of this command
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $this->output = $output;
    $this->debug = $output->isDebug();
    $this->getApplication()->container['output'] = $output;

    try {
      foreach ($this->getAccounts() as $account) {
          $connection = $this->connect($account);
          $this->processEmailsForAccount($account, $connection);
      }
    } catch (\Throwable $e) {
        $this->output->writeln('Error: ' . $e->getMessage());
        return Command::FAILURE;
    }

    return Command::SUCCESS;
  }

  /**
   * Read and process the emails for the specified account
   *
   * If one of the pipes throws an exception then we stop processing the
   * the email and move on to the next one and do not delete the email. This
   * means that the next run, it will pick up this email and attempt to
   * process it again. If the problem hasn't been fixed, then it will run all
   * pipes again until a failure occurs.
   */
  protected function processEmailsForAccount(array $account, Connection $connection): void
  {
    $mailbox = $connection->getMailbox($account['mailbox']);

    foreach ($mailbox->getMessages() as $message) {
      if ($message->isDeleted()) {
        continue;
      }

      if (!empty($account['validSenders'])) {
        $sender = $message->getFrom();

        /*
         * Ignore this email if the sender is not in the list of valid senders
         */
        if (!in_array($sender->getAddress(), $account['validSenders'])) {
          $this->output->writeln('Ignoring email from ' . $sender->getAddress(), OutputInterface::VERBOSITY_DEBUG);
          continue;
        }

      }
      $email = Message::from($message->getRawMessage(), true);

      foreach ($account['pipes'] as $pipeName => $config) {
        $pipe = PipeFactory::create($pipeName);
      $successfulPipe = true;

        try {
            $pipe($email, $config, $this->debug);
        } catch (\Exception $e) {
            $this->output->writeln('Error piping email to ' . $pipeName . ': ' . $e->getMessage());
            $successfulPipe = false;
            break;
        }
      }

      if ($successfulPipe) {
          $message->delete();
      }
    }

    $connection->expunge();
  }


  /**
   * Connect to the specified email account
   */
  protected function connect(array $account): Connection
  {
    try {
      $domain = explode('@', $account['email'])[1];
      getmxrr($domain, $mxhosts, $mxweights);
      $server = new Server($mxhosts[0]);
      $connection = $server->authenticate($account['email'], $account['password']);
    } catch (\Exception $e) {
      $this->output->writeln('Error connecting to ' . $account['email'] . ': ' . $e->getMessage());
      throw new Exception('Error connecting to ' . $account['email'] . ': ' . $e->getMessage());
    }

    return $connection;
  }



  protected function getAccounts(): array
  {
      return $this->getApplication()->container['accounts'];
  }
}
